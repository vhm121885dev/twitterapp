package mx.vhm.com.twitterfeed;

import android.app.Application;
import android.util.Log;

import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;

/**
 * Created by victorhernandez on 27/04/18.
 */

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        initializeApiTwitter();
    }

    private void initializeApiTwitter(){
        TwitterConfig config = new TwitterConfig.Builder(this)
                .logger(new DefaultLogger(Log.DEBUG))
//                .twitterAuthConfig(new TwitterAuthConfig("YTKRQXoU6y8pMiH6fVoCMERlg",
//                        "pNPPipoQUE5BpY4b27u0ZXaweypuCn2JThZ6h65PR4qJvCFm05"))
               .twitterAuthConfig(new TwitterAuthConfig("h835hmmGNYsKqFxEts6keGvfC", "kefJbnzbspZksOxMgHFUBChHssSS3205SLGW2R2nWMKndJzLzW"))//pass Twitter API Key and Secret
                .debug(true)
                .build();
        Twitter.initialize(config);
    }
}
