package mx.vhm.com.twitterfeed.presentation.ui.base;

/**
 * Created by victorhernandez on 27/04/18.
 */

public interface BasePresenter<T> {

    T getView();

    void setView(T view);

}
