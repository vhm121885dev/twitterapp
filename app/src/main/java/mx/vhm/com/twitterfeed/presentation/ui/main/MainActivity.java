package mx.vhm.com.twitterfeed.presentation.ui.main;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.tweetui.SearchTimeline;
import com.twitter.sdk.android.tweetui.TimelineResult;
import com.twitter.sdk.android.tweetui.TweetTimelineRecyclerViewAdapter;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import mx.vhm.com.twitterfeed.R;
import mx.vhm.com.twitterfeed.presentation.ui.base.BaseActivity;

public class MainActivity extends BaseActivity implements MainContract.View {

    @BindView(R.id.recycler_tweets)
    public RecyclerView recyclerTweets;

    @BindView(R.id.edt_query)
    public EditText searchQuery;

    @BindView(R.id.swipe_refresh_tweets)
    public SwipeRefreshLayout swipeRefreshLayout;

    private TweetTimelineRecyclerViewAdapter adapter;

    @Override
    protected int layoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void initComponents() {
        ButterKnife.bind(this);
        setUpSwipeRefreshLayout();
        setUpRecyclerView();
        setUpSearchQuery();
    }

    @Override
    public void setUpRecyclerView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerTweets.setLayoutManager(linearLayoutManager);
    }

    @Override
    public void setUpSearchQuery() {

        searchQuery.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_SEARCH) {
                    String searchQuery = textView.getText().toString().trim();
                    if (!TextUtils.isEmpty(searchQuery)) {
                        hideKeyboard(textView);
                        doTwitterSearch(searchQuery);
                    } else {
                        showError(getString(R.string.error_epmty_search));
                    }

                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void doTwitterSearch(String query) {
        SearchTimeline searchTimeline = new SearchTimeline.Builder()
                .query(query)
                .languageCode(Locale.ENGLISH.getLanguage())
                .maxItemsPerRequest(50)
                .build();

        adapter = new TweetTimelineRecyclerViewAdapter.Builder(this)
                .setTimeline(searchTimeline)
                .setOnActionCallback(new Callback<Tweet>() {
                    @Override
                    public void success(Result<Tweet> result) {
                    }

                    @Override
                    public void failure(TwitterException exception) {
                    }
                })
                .setViewStyle(R.style.tw__TweetLightWithActionsStyle)
                .build();

        recyclerTweets.setAdapter(adapter);
    }

    @Override
    public void setUpSwipeRefreshLayout() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (adapter == null)
                    return;

                swipeRefreshLayout.setRefreshing(true);
                adapter.refresh(new Callback<TimelineResult<Tweet>>() {
                    @Override
                    public void success(Result<TimelineResult<Tweet>> result) {
                        swipeRefreshLayout.setRefreshing(false);
                        showMessage(getString(R.string.refresh));
                    }

                    @Override
                    public void failure(TwitterException exception) {
                        showMessage(getString(R.string.error_search_tweets));
                    }
                });
            }
        });
    }

    @Override
    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

}
