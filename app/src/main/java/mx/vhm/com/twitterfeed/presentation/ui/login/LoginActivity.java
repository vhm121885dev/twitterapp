package mx.vhm.com.twitterfeed.presentation.ui.login;

import android.content.Intent;

import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import mx.vhm.com.twitterfeed.presentation.ui.base.BaseActivity;
import mx.vhm.com.twitterfeed.utils.PreferenceHelper;
import mx.vhm.com.twitterfeed.R;
import mx.vhm.com.twitterfeed.presentation.ui.main.MainActivity;

/**
 * Created by victorhernandez on 27/04/18.
 */

public class LoginActivity extends BaseActivity implements LoginContract.View {

    @BindView(R.id.login_button)
    public TwitterLoginButton btnLogin;
    private PreferenceHelper preferenceHelper;
    private LoginContract.Presenter presenter;

    @Override
    protected int layoutId() {
        return R.layout.activity_login;
    }

    @Override
    protected void initComponents() {
        sessionCheck();
        ButterKnife.bind(this);
        presenter = new LoginPresenterImpl(this);
        setCallbackLoginButton();
    }

    @Override
    public void sessionCheck() {
        preferenceHelper = new PreferenceHelper(this);
        if (preferenceHelper.getUserId() != 0) {
            startMainActivity();
            return;
        }
    }

    @Override
    public void setCallbackLoginButton() {
        btnLogin.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                presenter.proecessResultloginTwitter(result);
            }

            @Override
            public void failure(TwitterException exception) {
                showErrorLogin();
            }
        });
    }

    @Override
    public void startMainActivity() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    public void setPreferencesLogin(long userId, String userName) {
        preferenceHelper.saveUserId(userId);
        preferenceHelper.saveScreenName(userName);
    }

    @Override
    public void showErrorLogin() {
        showError(getString(R.string.error_login));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        btnLogin.onActivityResult(requestCode, resultCode, data);
    }

}
