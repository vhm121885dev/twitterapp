package mx.vhm.com.twitterfeed.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by victorhernandez on 27/04/18.
 */

public class PreferenceHelper {

    private static final String PREF_KEY = "login_prefs";
    private static final String USER_ID = "user_id";
    private static final String SCREEN_NAME = "screen_name";

    private SharedPreferences sharedPreferences;

    public PreferenceHelper(Context context) {
        sharedPreferences = context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE);
    }

    public void saveUserId(Long userId) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(USER_ID, userId);
        editor.apply();
    }

    public Long getUserId() {
        return sharedPreferences.getLong(USER_ID, 0);
    }

    public void saveScreenName(String screenName) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(SCREEN_NAME, screenName);
        editor.apply();
    }

    public String getScreenName() {
        return sharedPreferences.getString(SCREEN_NAME, "");
    }
}
