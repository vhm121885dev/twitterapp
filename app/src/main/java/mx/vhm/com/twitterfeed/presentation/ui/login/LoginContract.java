package mx.vhm.com.twitterfeed.presentation.ui.login;

import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterSession;

import mx.vhm.com.twitterfeed.presentation.ui.base.BasePresenter;
import mx.vhm.com.twitterfeed.presentation.ui.base.IBaseView;

/**
 * Created by victorhernandez on 27/04/18.
 */

public class LoginContract {

    interface Presenter extends BasePresenter<View> {
        void proecessResultloginTwitter(Result<TwitterSession> result);
    }

    interface View extends IBaseView {
        void sessionCheck();
        void setCallbackLoginButton();
        void startMainActivity();
        void setPreferencesLogin(long userId, String userName);
        void showErrorLogin();
    }
}
