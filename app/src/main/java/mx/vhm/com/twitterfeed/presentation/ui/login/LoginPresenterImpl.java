package mx.vhm.com.twitterfeed.presentation.ui.login;

import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterSession;

/**
 * Created by victorhernandez on 27/04/18.
 */

public class LoginPresenterImpl implements LoginContract.Presenter {

    private LoginContract.View view;

    public LoginPresenterImpl(LoginContract.View view) {
        this.view = view;
    }

    @Override
    public LoginContract.View getView() {
        return this.view;
    }

    @Override
    public void setView(LoginContract.View view) {
        this.view = view;
    }

    @Override
    public void proecessResultloginTwitter(Result<TwitterSession> result) {
        TwitterSession twitterSession = result.data;
        if (twitterSession != null) {
            getView().setPreferencesLogin(twitterSession.getUserId(), twitterSession.getUserName());
            getView().startMainActivity();
        } else {
            getView().showErrorLogin();
        }
    }
}
