package mx.vhm.com.twitterfeed.presentation.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import butterknife.ButterKnife;
import mx.vhm.com.twitterfeed.R;
import mx.vhm.com.twitterfeed.utils.UtilsUi;

/**
 * Created by victorhernandez on 27/04/18.
 */

public abstract class BaseActivity extends AppCompatActivity implements IBaseView {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layoutId());
        ButterKnife.bind(this);
        initComponents();
    }

    protected abstract int layoutId();

    protected abstract void initComponents();

    @Override
    public boolean isOnline() {
        return UtilsUi.isOnline(this);
    }

    @Override
    public void showError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT);
    }

    @Override
    public void showMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT);
    }

}
