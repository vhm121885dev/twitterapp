package mx.vhm.com.twitterfeed.presentation.ui.main;

import mx.vhm.com.twitterfeed.presentation.ui.base.BasePresenter;
import mx.vhm.com.twitterfeed.presentation.ui.base.IBaseView;

/**
 * Created by victorhernandez on 27/04/18.
 */

public class MainContract {

    interface Presenter extends BasePresenter<View> {
    }

    interface View extends IBaseView {
        void setUpRecyclerView();
        void setUpSearchQuery();
        void doTwitterSearch(String query);
        void setUpSwipeRefreshLayout();
        void hideKeyboard(android.view.View view);
    }
}
