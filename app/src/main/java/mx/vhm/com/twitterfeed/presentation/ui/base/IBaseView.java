package mx.vhm.com.twitterfeed.presentation.ui.base;

/**
 * Created by victorhernandez on 27/04/18.
 */

public interface IBaseView {
    boolean isOnline();
    void showError(String error);
    void showMessage(String msg);
}
